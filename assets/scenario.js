export const generateResult = ({ interests, }) => {
  const paymentTables = []
  for (let i = 0; i < interests.length; i++) {
    const paymentTable = generatePaymentTable()
    paymentTables.push(paymentTable)
  }

  return {
    paymentTables
  }
}

function generatePaymentTable() {
  return {

  }
}

function percent(num) { return num / 100 }
function monthly(num) { return num / 1200 }
function month(year) { return year * 12 }
function year(month) { return Math.floor(month / 12) }
function compound(rate, period) { return Math.pow(1 + rate, period) }
function growth(initial, rate, period) { return initial * this.compound(rate, period) }
function grow(initial, rate) { return initial * (1+rate) }
function discount(initial, rate) { return initial * (1-rate) }
