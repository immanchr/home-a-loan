state = () => ({
  scenarios: []
})

mutations = {
    add(state, scenario) {
        state.scenarios.push(scenario)
    },
    del(state, index) {
        state.scenarios.splice(index, 1)
    }
}